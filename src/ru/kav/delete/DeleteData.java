package ru.kav.delete;

import java.util.concurrent.ConcurrentHashMap;
import java.util.*;

public class DeleteData {
    public static void main(String args[]) {

        Set<String> Arr = Collections.newSetFromMap(new ConcurrentHashMap<>());
        Arr.addAll(Arrays.asList("h", "i", "q", "k", "l", "m", "o"));

        Set<String> Arr2 = Collections.newSetFromMap(new ConcurrentHashMap<>());
        Arr2.addAll(Arrays.asList("p", "q", "r", "s", "q", "u"));

        for (String a : Arr) {
            if (check(a)) {
                Arr.remove(a);
            }
        }
        for (String a : Arr2) {
            if (check(a)) {
                Arr2.remove(a);
            }
        }

        System.out.println(Arr);
        System.out.println(Arr2);
    }

    public static boolean check(String str) {
        return str.equals("q");
    }

}
